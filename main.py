"""Read a csv file and insert data into database."""

import asyncio
import sys
import typing
from pathlib import Path

from sma_backend.api.v0.services.csv_reader import read_csv
from sma_backend.db import async_session_maker


def exit_error(message: str = '') -> None:
    """Exit error message."""
    print('Error:', message)
    sys.exit(1)


def parse_args() -> typing.Dict[str, typing.Any]:
    """Retrieve the path to the csv."""
    args = sys.argv

    if len(args) < 2:
        exit_error('missing CSV file path')

    if len(args) > 3:
        exit_error('too many arguments')

    return {
        'file_path': Path(args[1])
    }


async def main() -> None:
    """Read the csv using the path obtained."""
    args = parse_args()
    async with async_session_maker() as db:
        await read_csv(db=db, file=args['file_path'])


if __name__ == '__main__':
    asyncio.run(main())
