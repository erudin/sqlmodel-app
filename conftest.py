import os

import pytest
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

# Signal that we are running tests before importing anything related to the project
os.environ.setdefault('SMA_TESTING', 'true')

from sma_backend.app.asgi import app
from sma_backend.db import async_session_maker
from sma_backend.db import init_without_migrations


@pytest.fixture
async def async_client(db: AsyncSession):
    async with AsyncClient(app=app, base_url='http://127.0.0.1:8000/v0') as client:
        yield client


@pytest.fixture
async def db():
    await init_without_migrations(drop_all=True)

    async with async_session_maker() as db:
        yield db
