"""Utils."""

import typing

from sqlmodel import SQLModel


def asdict(model: SQLModel) -> typing.Dict[str, typing.Any]:
    """
    Remove private members from a model dict.

    This is something that Pydantic and SQLModel lacks.
    """
    dct: typing.Dict[str, typing.Any] = dict(model)

    # Remove private members
    keys_to_remove = set(filter(lambda key: key.startswith('_'), dct.keys()))
    for key in keys_to_remove:
        del dct[key]

    return dct
