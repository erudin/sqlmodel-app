# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Project utilities."""

import os
from functools import lru_cache

from . import settings


@lru_cache()
def get_project_version() -> str:
    """Get app version from PyProject TOML file efficiently.

    :return: Project version or default value 0.1.0.
    """
    project_version = '0.1.0'  # Default version when not found
    pyproject = os.path.join(os.path.dirname(settings.BASE_DIR), 'pyproject.toml')
    with open(pyproject, 'rt') as toml_file:
        for line in toml_file:
            if 'version' in line:
                _, version = line.split(' = ')
                project_version = version.strip()[1:-1]  # Remove double quotes
                break

    return project_version


@lru_cache()
def get_project_internal_version() -> str:
    """Get internal project version (might not be suitable for public display).

    :return: Project internal version extracted from `.version` file or empty string.
    """
    project_internal_version = ''  # Default empty
    version_path = os.path.join(settings.BASE_DIR, 'version')
    try:
        with open(version_path, 'rt') as version_file:
            project_internal_version = version_file.readline().rstrip('\n')
    except FileNotFoundError:
        pass  # Do nothing

    return project_internal_version


def get_app_base_url() -> str:
    """Get the application base full URL including protocol."""
    host = settings.ALLOWED_HOSTS[0]
    # HTTPS protocol hardcoded :)
    return f'https://{host}{settings.API_PREFIX}'.rstrip('/')
