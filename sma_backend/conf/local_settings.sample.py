# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Local settings sample file for development. Rename to `local_settings.py`.

Avoid using a file like this in production in favor of env vars.
"""

import typing

from pydantic import BaseSettings


class LocalSettings(BaseSettings):  # Keep the class name!
    """Local settings for the app."""

    DEBUG: bool = True
    DEVELOPMENT_MODE: bool = False
    ENABLE_HTTPS_REDIRECT: bool = False
    ALLOWED_HOSTS: typing.List[str] = ['127.0.0.1']
    API_PREFIX: str = ''
    LOGLEVEL: str = 'DEBUG'
