# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Lazy project settings."""

from .configs import get_settings
from ..utils.functional import SimpleLazyObject


class LazySettings(SimpleLazyObject):
    """Lazy project settings."""

    def __init__(self):
        """Initialise lazy object with project settings."""
        super().__init__(get_settings)
