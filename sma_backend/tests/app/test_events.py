# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from unittest import IsolatedAsyncioTestCase
from unittest import mock

from ...app import events


class TestAppEvents(IsolatedAsyncioTestCase):

    @mock.patch.object(events, 'init_without_migrations')
    @mock.patch.object(events, 'settings')
    @mock.patch.object(events, 'logger')
    async def test_startup(
            self,
            mock_logger: mock.MagicMock,
            mock_settings: mock.MagicMock,
            mock_init_without_migrations: mock.AsyncMock,
    ) -> None:
        mock_settings.DEVELOPMENT_MODE = False

        await events.startup()
        mock_logger.info.assert_called_once()
        mock_logger.debug.assert_called_once()
        mock_init_without_migrations.assert_called_once_with()

    @mock.patch.object(events, 'init_without_migrations')
    @mock.patch.object(events, 'settings')
    @mock.patch.object(events, 'logger')
    async def test_startup_dev_mode(
            self,
            mock_logger: mock.MagicMock,
            mock_settings: mock.MagicMock,
            mock_init_without_migrations: mock.AsyncMock,
    ) -> None:
        mock_settings.DEVELOPMENT_MODE = True

        await events.startup()
        mock_logger.info.assert_called_once()
        mock_logger.debug.assert_called_once()
        mock_logger.warning.assert_called_once()
        mock_init_without_migrations.assert_called_once_with()

    async def test_shutdown(self):
        await events.shutdown()
