# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from unittest import IsolatedAsyncioTestCase
from unittest import mock

from ...api import shortcuts


class TestAPIShortcuts(IsolatedAsyncioTestCase):

    @mock.patch.object(shortcuts, 'async_session_maker')
    async def test_get_db(self, mock_async_session: mock.MagicMock):
        db = yield shortcuts.get_db()
        self.assertIs(mock_async_session, db)
