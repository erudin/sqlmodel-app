import pytest
from fastapi import status
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from sma_backend.api.v0.schemas import MetricCreate
from sma_backend.api.v0.schemas import ValueDefinitionCreate
from sma_backend.api.v0.selectors.value_definition import get_value_definition
from sma_backend.api.v0.services.metric import create_metric
from sma_backend.api.v0.services.value_definition import create_value_definition
from sma_backend.utils.models import asdict


@pytest.mark.asyncio
async def test_get_empty_value_definitions(async_client: AsyncClient):
    response = await async_client.get('/value_definitions/')
    assert status.HTTP_200_OK == response.status_code
    assert [] == response.json()


@pytest.mark.asyncio
async def test_get_value_definitions(async_client: AsyncClient, db: AsyncSession):
    metric = await create_metric(
        db=db,
        metric=MetricCreate(code='test_code1', description='test_description1'),
    )
    value_definition = await create_value_definition(
        db=db,
        value_definition=ValueDefinitionCreate(
            value_label='test_value',
            value_type='test_type',
            metric_id=metric.id,
        ),
    )
    response = await async_client.get('/value_definitions/')
    assert response.status_code == status.HTTP_200_OK
    assert [value_definition.dict()] == response.json()


@pytest.mark.asyncio
async def test_get_value_definition_with_wrong_id(async_client: AsyncClient):
    response = await async_client.get('/value_definitions/99/')
    assert status.HTTP_404_NOT_FOUND == response.status_code


@pytest.mark.asyncio
async def test_get_value_definition(async_client: AsyncClient, db: AsyncSession):
    metric = await create_metric(
        db=db,
        metric=MetricCreate(code='test_code2', description='test_description2'),
    )
    value_definition = await create_value_definition(
        db=db,
        value_definition=ValueDefinitionCreate(
            value_label='test_value',
            value_type='test_type',
            metric_id=metric.id,
        ),
    )
    value_definition = await get_value_definition(
        db=db,
        value_definition_id=value_definition.id,
    )
    vdef_dict = asdict(value_definition)

    response = await async_client.get(f'/value_definitions/{value_definition.id}/')
    assert response.status_code == status.HTTP_200_OK
    response_data = response.json()
    assert vdef_dict == response_data


@pytest.mark.asyncio
async def test_create_value_definition(async_client: AsyncClient, db):
    metric = await create_metric(
        db=db,
        metric=MetricCreate(code='test_code3', description='test_description3'),
    )
    value_definition = ValueDefinitionCreate(
        value_label='test_value',
        value_type='test_type',
        metric_id=metric.id,
    )

    response = await async_client.post(
        '/value_definitions/',
        json=value_definition.dict(),
    )
    assert response.status_code == status.HTTP_201_CREATED

    response_data = response.json()
    assert 'id' in response_data
    assert value_definition == ValueDefinitionCreate(**response_data)
