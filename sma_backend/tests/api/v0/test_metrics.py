import pytest
from fastapi import status
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from sma_backend.api.v0.schemas import MetricCreate
from sma_backend.api.v0.schemas import ValueDefinitionCreate
from sma_backend.api.v0.selectors.metric import get_metric
from sma_backend.api.v0.services.metric import create_metric
from sma_backend.api.v0.services.value_definition import create_value_definition
from sma_backend.utils.models import asdict


@pytest.mark.asyncio
async def test_get_empty_metrics(async_client: AsyncClient):
    response = await async_client.get('/metrics/')
    assert status.HTTP_200_OK == response.status_code
    assert [] == response.json()


@pytest.mark.asyncio
async def test_get_metrics(async_client: AsyncClient, db: AsyncSession):
    metric = await create_metric(
        db=db,
        metric=MetricCreate(code='test_code1', description='test_description1'),
    )
    response = await async_client.get('/metrics/')
    assert response.status_code == status.HTTP_200_OK
    assert [metric.dict()] == response.json()


@pytest.mark.asyncio
async def test_get_metric_with_wrong_id(async_client: AsyncClient):
    response = await async_client.get('/metrics/99/')
    assert status.HTTP_404_NOT_FOUND == response.status_code


@pytest.mark.asyncio
async def test_get_metric(async_client: AsyncClient, db: AsyncSession):
    metric = await create_metric(
        db=db,
        metric=MetricCreate(code='test_code2', description='test_description2'),
    )
    await create_value_definition(
        db=db,
        value_definition=ValueDefinitionCreate(
            value_label='test_value',
            value_type='test_type',
            metric_id=metric.id,
        ),
    )
    metric = await get_metric(db=db, metric_id=metric.id)
    metric_dict = asdict(metric)

    response = await async_client.get(f'/metrics/{metric.id}/')
    assert response.status_code == status.HTTP_200_OK
    response_data = response.json()
    assert metric_dict == response_data


@pytest.mark.asyncio
async def test_create_metric(async_client: AsyncClient):
    metric = MetricCreate(code='test_code3', description='test_description3')

    response = await async_client.post('/metrics/', json=metric.dict())
    assert response.status_code == status.HTTP_201_CREATED

    response_data = response.json()
    assert 'id' in response_data
    assert metric == MetricCreate(**response_data)
