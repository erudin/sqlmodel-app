# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from unittest import IsolatedAsyncioTestCase
from unittest import mock

from ...db import initialization


class TestDBInitialization(IsolatedAsyncioTestCase):

    @mock.patch.object(initialization, 'get_engine')
    async def test_init_without_migrations(self, mock_get_engine):
        await initialization.init_without_migrations()
        mock_get_engine.assert_called_once()
        # ToDo
