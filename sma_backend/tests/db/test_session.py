# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Test DB session maker functions."""

from unittest import IsolatedAsyncioTestCase
from unittest import mock

from sqlalchemy.ext.asyncio import AsyncSession

from ...db import session


class TestDBSession(IsolatedAsyncioTestCase):

    @mock.patch.object(session, 'create_async_engine')
    def test_get_engine(self, mock_create_engine: mock.MagicMock) -> None:
        mock_engine = mock.MagicMock()
        mock_create_engine.return_value = mock_engine

        self.assertIs(session.get_engine(), mock_engine)
        mock_create_engine.assert_called_once()

    @mock.patch.object(session, 'sessionmaker')
    def test_create_session(self, mock_sessionmaker: mock.MagicMock) -> None:
        mock_engine = mock.MagicMock()
        mock_session = mock.MagicMock()
        mock_sessionmaker.return_value = mock_session

        self.assertIs(session.create_session(mock_engine), mock_session)
        mock_sessionmaker.assert_called_once_with(
            mock_engine,
            class_=AsyncSession,
            expire_on_commit=False,
        )
