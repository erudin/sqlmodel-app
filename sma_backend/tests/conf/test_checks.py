# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from unittest import TestCase
from unittest import mock

from ...conf import checks


class TestConfChecks(TestCase):

    def test_configure_settings_dev_mode(self):
        settings = mock.MagicMock()
        settings.DEVELOPMENT_MODE = True
        checks.configure_settings(settings)
        self.assertEqual(settings.DEBUG, True)

    def test_configure_settings_allowed_origins_no_frontend(self):
        settings = mock.MagicMock()
        settings.DEVELOPMENT_MODE = False
        settings.ALLOW_FRONTEND_LOCAL = False
        settings.ALLOWED_HOSTS = ['1.2.3.4', '5.6.7.8']

        checks.configure_settings(settings)
        self.assertEqual(
            settings.ALLOWED_ORIGINS,
            ['https://1.2.3.4', 'https://5.6.7.8'],
        )

    def test_configure_settings_allowed_origins_with_frontend(self):
        settings = mock.MagicMock()
        settings.DEVELOPMENT_MODE = False
        settings.ALLOW_FRONTEND_LOCAL = True
        settings.ALLOWED_HOSTS = ['1.2.3.4', '5.6.7.8']

        checks.configure_settings(settings)
        self.assertEqual(
            settings.ALLOWED_ORIGINS,
            ['http://127.0.0.1:3000', 'https://1.2.3.4', 'https://5.6.7.8'],
        )

    def test_configure_settings_database_uri_no_db(self):
        settings = mock.MagicMock()
        settings.DEVELOPMENT_MODE = False
        settings.DATABASE_NAME = 'name'
        settings.DATABASE_USER = ''

        checks.configure_settings(settings)
        self.assertEqual(settings.DATABASE_URI, 'sqlite+aiosqlite:///./name')

    def test_configure_settings_database_uri_with_db_no_pwd(self):
        settings = mock.MagicMock()
        settings.DEVELOPMENT_MODE = False
        settings.DATABASE_DIALECT = 'dialect'
        settings.DATABASE_NAME = 'name'
        settings.DATABASE_USER = 'user'
        settings.DATABASE_HOST = 'host'
        settings.DATABASE_PORT = '1234'
        settings.DATABASE_PARAMS = 'param=1'
        settings.DATABASE_PASSWORD = ''

        checks.configure_settings(settings)
        self.assertEqual(
            settings.DATABASE_URI,
            'dialect://user@host:1234/name?param=1',
        )

    def test_configure_settings_database_uri_with_db_with_pwd(self):
        settings = mock.MagicMock()
        settings.DEVELOPMENT_MODE = False
        settings.DATABASE_DIALECT = 'dialect'
        settings.DATABASE_NAME = 'name'
        settings.DATABASE_USER = 'user'
        settings.DATABASE_HOST = 'host'
        settings.DATABASE_PORT = '1234'
        settings.DATABASE_PARAMS = 'param=1'
        settings.DATABASE_PASSWORD = 'passwd'

        checks.configure_settings(settings)
        self.assertEqual(
            settings.DATABASE_URI,
            'dialect://user:passwd@host:1234/name?param=1',
        )
