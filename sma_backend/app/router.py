# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Instantiate application routers."""

from .asgi import app
from .routing import configure_app_routers
from ..api.root import router as api_router_root

configure_app_routers(app, api_router_root)
