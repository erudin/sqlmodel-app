# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Application events."""

import logging

from .asgi import app
from ..conf import settings
from ..conf.utils import get_project_internal_version
from ..conf.utils import get_project_version
from ..db import init_without_migrations

logger = logging.getLogger(__name__)


@app.on_event('startup')
async def startup() -> None:
    """Initialize the application."""
    version, internal_version = get_project_version(), get_project_internal_version()
    logger.info(
        'Starting %s (%s) v%s (%s)...',
        settings.APP_DESCRIPTION,
        settings.APP_IDENTIFIER,
        version,
        internal_version,
    )
    logger.debug('Settings: %s', settings)
    if settings.DEVELOPMENT_MODE:
        logger.warning('!!! DEVELOPMENT MODE IS ACTIVE !!!')

    # ToDo: use migrations
    await init_without_migrations()


@app.on_event('shutdown')
async def shutdown() -> None:
    """Finalize the application."""
    # Write whatever you need to shut down the application, such as disconnecting
    # from the database. Don't forget to update the corresponding test!.
