# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""App exception handlers."""

from fastapi import Request
from fastapi import status
from fastapi.responses import JSONResponse
from sqlalchemy.exc import NoResultFound

from .asgi import app
from ..exceptions import ValidationError


@app.exception_handler(ValidationError)
async def validation_error_exception_handler(
        request: Request,
        exc: ValidationError,
) -> JSONResponse:
    """Handle ValidationError exceptions properly."""
    return JSONResponse(
        status_code=status.HTTP_400_BAD_REQUEST,
        content={'detail': f'{exc}'},
    )


@app.exception_handler(NoResultFound)
async def object_not_found_exception_handler(
        request: Request,
        exc: NoResultFound,
) -> JSONResponse:
    """Handle ObjectNotFound exceptions properly."""
    return JSONResponse(
        status_code=status.HTTP_404_NOT_FOUND,
        content={'detail': f'{exc}'},
    )
