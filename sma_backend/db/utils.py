# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Database related utilities."""

from datetime import datetime
from datetime import timezone

from sqlalchemy.orm import Session

from ..models.functions import utcnow as fn_utcnow


async def utcnow(
        db: Session,
        *,
        naive: bool = False,
        aware: bool = False,
) -> datetime:
    """Get current datetime from the database in UTC.

    This function returns the value from the database as-is: either aware or naive.
    Set the corresponding option to force one or the other. In any case the
    datetime value is ensured to be in UTC.

    Note that this function executes a query to the database.

    :param db: Database session.
    :param naive: [Optional] Force the datetime as naive.
    :param aware: [Optional] Force the datetime as aware.

    :return: Current datetime from database in UTC (aware or naive).
    """
    if naive and aware:
        raise ValueError('can not force datetime to be naive and aware')

    qs = db.query(fn_utcnow()).one()
    dt: datetime = qs[0]
    # SQLite does not implement aware datetime. I tried cheating the engine by
    # returning an aware datetime string but it didn't work, I guess SQLAlchemy
    # driver doesn't even try to make it aware. So, we need to check here if
    # given value is aware or not.
    is_naive = dt.utcoffset() is None
    if is_naive and aware:
        dt = dt.replace(tzinfo=timezone.utc)
    elif not is_naive and naive:
        dt = dt.replace(tzinfo=None)

    return dt
