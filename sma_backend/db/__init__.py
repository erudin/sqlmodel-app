# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Expose database related classes and functions."""

from .initialization import init_without_migrations
from .session import create_session
from .session import get_engine
# Import all models, so that Base has them before being imported by Alembic (if used)
# noinspection PyUnresolvedReferences
from ..models import *  # noqa

async_session_maker = create_session(get_engine())

__all__ = (
    'async_session_maker',
    'init_without_migrations',
)
