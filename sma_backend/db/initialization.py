# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Function to initialise the dbs without migrations."""

from sqlmodel import SQLModel

from .session import get_engine


async def init_without_migrations(*, drop_all: bool = False) -> None:
    """Initialise db without migrations."""
    engine = get_engine()

    async with engine.begin() as conn:
        if drop_all:
            await conn.run_sync(SQLModel.metadata.drop_all)

        await conn.run_sync(SQLModel.metadata.create_all)
