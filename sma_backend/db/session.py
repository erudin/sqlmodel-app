# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Database connection session."""

from sqlalchemy.ext.asyncio import AsyncEngine
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker

from ..conf import settings


def get_engine() -> AsyncEngine:
    """Get the database engine."""
    # ToDo: find the `timeout` parameter, I couldn't find any (pool_timeout
    #  doesn't work for non-pools)
    kwargs = {
        'echo': True if settings.DEVELOPMENT_MODE else False,
    }
    if settings.DATABASE_URI.startswith('sqlite'):
        kwargs['connect_args'] = {'check_same_thread': False}

    kwargs.setdefault('future', True)
    return create_async_engine(settings.DATABASE_URI, **kwargs)


def create_session(engine: AsyncEngine) -> sessionmaker:
    """Create the database session for the given engine."""
    return sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)
