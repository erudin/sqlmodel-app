# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Helper functions and classes definition for API endpoints."""

import functools
import typing

from fastapi import HTTPException
from fastapi import status
from sqlalchemy.ext.asyncio import AsyncSession

from ..db import async_session_maker


async def get_db() -> typing.Generator[AsyncSession, None, None]:
    """DB session dependency for FastAPI."""
    async with async_session_maker() as session:
        yield session


def catch(
        exc_classes: typing.Union[  # noqa: TAE002
            typing.Type[Exception],
            typing.Tuple[typing.Type[Exception], ...],
        ],
        *,
        status_code: int = status.HTTP_400_BAD_REQUEST,
        use_exception_message: bool = True,
) -> typing.Callable:
    """Catch any exception of given class/es raising an HTTPException instead."""

    def decorator(function):
        """Catch any exception raising an HTTPException instead."""

        @functools.wraps(function)
        async def wrapper(*args, **kwargs):
            """Wrap decorated function."""
            try:
                return await function(*args, **kwargs)
            except exc_classes as exc:
                detail = (f'{exc}' or None) if use_exception_message else None
                raise HTTPException(status_code=status_code, detail=detail) from exc

        return wrapper

    return decorator
