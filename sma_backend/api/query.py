# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Query classes for the endpoints."""

from fastapi import Query


class CommonQueryParameters:
    """Common query parameters for the endpoints."""

    MAX_LIMIT: int = 100

    def __init__(
            self,
            search: str = Query(None, min_length=3, regex=r'^[a-zA-Z0-9 .,-_]+$'),
            offset: int = 0,
            limit: int = 20,
    ) -> None:
        """Initialise the parameters."""
        self.search = search
        if limit < self.MAX_LIMIT:
            self.limit = limit
        else:
            self.limit = self.MAX_LIMIT

        if offset > self.limit:
            self.offset = self.limit
        else:
            self.offset = offset
