"""Business logic around fetching value definitions."""

import typing

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload
from sqlmodel import select

from sma_backend import models


async def get_value_definitions(
        *,
        db: AsyncSession,
) -> typing.List[models.ValueDefinition]:
    """Retrieve all value definition objects."""
    objs = await db.execute(select(models.ValueDefinition))
    return objs.scalars().all()


async def get_value_definition(
        *,
        db: AsyncSession,
        value_definition_id: int,
) -> models.ValueDefinition:
    """Retrieve a given value definition object."""
    obj = await db.execute(
        select(
            models.ValueDefinition,
        ).where(
            models.ValueDefinition.id == value_definition_id,
        ).options(
            selectinload(models.ValueDefinition.metric),
        ),
    )
    return obj.scalars().one()
