"""Business logic around fetching metrics."""

import typing

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload
from sqlmodel import select

from sma_backend import models


async def get_metrics(*, db: AsyncSession) -> typing.List[models.Metric]:
    """Retrieve all metric objects."""
    objs = await db.execute(select(models.Metric))
    return objs.scalars().all()


async def get_metric(*, db: AsyncSession, metric_id: int) -> models.Metric:
    """Retrieve a given metric object."""
    obj = await db.execute(
        select(
            models.Metric,
        ).where(
            models.Metric.id == metric_id,
        ).options(
            selectinload(models.Metric.value_definitions),
        ),
    )
    return obj.scalars().one()
