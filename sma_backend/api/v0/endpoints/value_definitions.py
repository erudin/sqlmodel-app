"""Value Definition endpoints."""

import typing

from fastapi import APIRouter
from fastapi import Depends
from fastapi import status
from sqlalchemy.ext.asyncio import AsyncSession

from sma_backend import models
from .. import schemas
from .. import selectors
from .. import services
from ...shortcuts import get_db

router = APIRouter()


@router.post(
    '/',
    response_model=schemas.ValueDefinitionRead,
    status_code=status.HTTP_201_CREATED,
    responses={status.HTTP_201_CREATED: {}},
)
async def create_value_definition(
        *,
        db: AsyncSession = Depends(get_db),
        value_definition: schemas.ValueDefinitionCreate,
) -> models.ValueDefinition:
    """Create a value definition."""
    return await services.value_definition.create_value_definition(
        db=db, value_definition=value_definition,
    )


@router.get('/', response_model=typing.List[schemas.ValueDefinitionRead])
async def read_value_definitions(
        *,
        db: AsyncSession = Depends(get_db),
) -> typing.List[models.ValueDefinition]:
    """Retrieve all value definitions."""
    return await selectors.value_definition.get_value_definitions(db=db)


@router.get(
    '/{value_definition_id}/',
    response_model=schemas.ValueDefinitionReadWithMetric,
    responses={status.HTTP_404_NOT_FOUND: {}},
)
async def read_value_definition(
        *,
        db: AsyncSession = Depends(get_db),
        value_definition_id: int,
) -> models.ValueDefinition:
    """Retrieve a given value definition."""
    return await selectors.value_definition.get_value_definition(
        db=db,
        value_definition_id=value_definition_id,
    )


@router.put(
    '/{value_definition_id}/',
    response_model=schemas.ValueDefinitionReadWithMetric,
    responses={status.HTTP_404_NOT_FOUND: {}},
)
async def update_value_definition(
        *,
        db: AsyncSession = Depends(get_db),
        value_definition_id: int,
        value_definition: schemas.ValueDefinitionUpdate,
) -> models.ValueDefinition:
    """Update a given value definition."""
    obj = await selectors.value_definition.get_value_definition(
        db=db,
        value_definition_id=value_definition_id,
    )
    return await services.value_definition.update_value_definition(
        db=db,
        obj=obj,
        value_definition=value_definition,
    )


@router.delete(
    '/{value_definition_id}/',
    responses={status.HTTP_404_NOT_FOUND: {}, status.HTTP_204_NO_CONTENT: {}},
    status_code=status.HTTP_204_NO_CONTENT,
)
async def delete_value_definition(
        *,
        db: AsyncSession = Depends(get_db),
        value_definition_id: int,
) -> None:
    """Delete a given value definition."""
    obj = await selectors.value_definition.get_value_definition(
        db=db,
        value_definition_id=value_definition_id,
    )
    await services.value_definition.delete_value_definition(
        db=db, obj=obj,
    )
