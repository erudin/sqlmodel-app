"""Metric endpoints."""

import typing

from fastapi import APIRouter
from fastapi import Depends
from fastapi import status
from sqlalchemy.ext.asyncio import AsyncSession

from sma_backend import models
from .. import schemas
from .. import selectors
from .. import services
from ...shortcuts import get_db

router = APIRouter()


@router.post(
    '/',
    response_model=schemas.MetricRead,
    status_code=status.HTTP_201_CREATED,
    responses={status.HTTP_201_CREATED: {}},
)
async def create_metric(
        *,
        db: AsyncSession = Depends(get_db),
        metric: schemas.MetricCreate,
) -> models.Metric:
    """Create a  metric."""
    return await services.metric.create_metric(
        db=db, metric=metric,
    )


@router.get('/', response_model=typing.List[schemas.MetricRead])
async def read_metrics(
        *,
        db: AsyncSession = Depends(get_db),
) -> typing.List[models.Metric]:
    """Retrieve all metrics."""
    return await selectors.metric.get_metrics(db=db)


@router.get(
    '/{metric_id}/',
    response_model=schemas.MetricReadWithValueDefinitions,
    responses={status.HTTP_404_NOT_FOUND: {}},
)
async def read_metric(
        *,
        db: AsyncSession = Depends(get_db),
        metric_id: int,
) -> models.Metric:
    """Retrieve a given metric."""
    return await selectors.metric.get_metric(
        db=db,
        metric_id=metric_id,
    )


@router.put(
    '/{metric_id}/',
    response_model=schemas.MetricReadWithValueDefinitions,
    responses={status.HTTP_404_NOT_FOUND: {}},
)
async def update_metric(
        *,
        db: AsyncSession = Depends(get_db),
        metric_id: int,
        metric: schemas.MetricUpdate,
) -> models.Metric:
    """Update a given metric."""
    obj = await selectors.metric.get_metric(
        db=db,
        metric_id=metric_id,
    )
    return await services.metric.update_metric(
        db=db,
        obj=obj,
        metric=metric,
    )


@router.delete(
    '/{metric_id}/',
    responses={status.HTTP_404_NOT_FOUND: {}, status.HTTP_204_NO_CONTENT: {}},
    status_code=status.HTTP_204_NO_CONTENT,
)
async def delete_metric(
        *,
        db: AsyncSession = Depends(get_db),
        metric_id: int,
) -> None:
    """Delete a given metric."""
    obj = await selectors.metric.get_metric(
        db=db,
        metric_id=metric_id,
    )
    await services.metric.delete_metric(db=db, obj=obj)
