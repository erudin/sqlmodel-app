"""SMA schemas."""

import typing

from sma_backend.models import MetricBase
from sma_backend.models import ValueDefinitionBase


class MetricCreate(MetricBase):
    """Class model to create a Metric."""

    pass


class MetricRead(MetricBase):
    """Class model to read a Metric."""

    id: int  # noqa: A003


class MetricUpdate(MetricBase):
    """Class model to update a Metric."""

    description: typing.Optional[str] = None
    code: typing.Optional[str] = None


class ValueDefinitionCreate(ValueDefinitionBase):
    """Class model to create a Value Definition."""

    pass


class ValueDefinitionRead(ValueDefinitionBase):
    """Class model for Value Definition."""

    id: int  # noqa: A003


class ValueDefinitionUpdate(ValueDefinitionBase):
    """Class model for Value Definition."""

    value_label: typing.Optional[str] = None
    value_type: typing.Optional[str] = None
    metric_id: typing.Optional[int] = None


class MetricReadWithValueDefinitions(MetricRead):
    """Class model to read metrics with value definitions."""

    value_definitions: typing.List[ValueDefinitionRead] = []


class ValueDefinitionReadWithMetric(ValueDefinitionRead):
    """Class model to read value definition with metric."""

    metric: typing.Optional[MetricRead] = None
