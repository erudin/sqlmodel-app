"""Metric business logic."""

from sqlalchemy.ext.asyncio import AsyncSession

from sma_backend import models
from .. import schemas


async def create_metric(
        *,
        db: AsyncSession,
        metric: schemas.MetricCreate,
) -> models.Metric:
    """Create a metric object."""
    obj = models.Metric.from_orm(metric)
    db.add(obj)
    await db.commit()
    await db.refresh(obj)
    return obj


async def update_metric(
        *,
        db: AsyncSession,
        obj: models.Metric,
        metric: schemas.MetricUpdate,
) -> models.Metric:
    """Update a given metric object."""
    metric_data = metric.dict(exclude_unset=True)
    for key, value in metric_data.items():
        setattr(obj, key, value)
    db.add(obj)
    await db.commit()
    await db.refresh(obj)
    return obj


async def delete_metric(*, db: AsyncSession, obj: models.Metric):
    """Delete a given metric object."""
    await db.delete(obj)
    await db.commit()
