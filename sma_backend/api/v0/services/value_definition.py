"""Value definition business logic."""

from sqlalchemy.ext.asyncio import AsyncSession

from sma_backend import models
from .. import schemas


async def create_value_definition(
        *,
        db: AsyncSession,
        value_definition: schemas.ValueDefinitionCreate,
) -> models.ValueDefinition:
    """Create a value definition object."""
    obj = models.ValueDefinition.from_orm(value_definition)
    db.add(obj)
    await db.commit()
    await db.refresh(obj)
    return obj


async def update_value_definition(
        *,
        db: AsyncSession,
        obj: models.ValueDefinition,
        value_definition: schemas.ValueDefinitionUpdate,
) -> models.ValueDefinition:
    """Update a given value definition object."""
    value_definition_data = value_definition.dict(exclude_unset=True)
    for key, value in value_definition_data.items():
        setattr(obj, key, value)
    db.add(obj)
    await db.commit()
    await db.refresh(obj)
    return obj


async def delete_value_definition(
        *,
        db: AsyncSession,
        obj: models.ValueDefinition,
) -> None:
    """Delete a given value definition object."""
    await db.delete(obj)
    await db.commit()
