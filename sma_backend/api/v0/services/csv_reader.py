"""CSV reader to populate database."""

import csv
from pathlib import Path

from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from .metric import create_metric
from .value_definition import create_value_definition
from ..schemas import MetricCreate
from ..schemas import ValueDefinitionCreate


async def read_csv(*, db: AsyncSession, file: Path) -> None:
    """Read metric data from CSV."""
    with open(file, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            try:
                metric = await create_metric(
                    db=db,
                    metric=MetricCreate(
                        description=row['metric_description'],
                        code=row['metric_code'],
                    ),
                )
            except IntegrityError:
                await db.rollback()  # Discard object creation
                continue

            await create_value_definition(
                db=db,
                value_definition=ValueDefinitionCreate(
                    value_label=row['value_label'],
                    value_type=row['value_type'],
                    metric_id=metric.id,
                ),
            )
