# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""API v0.0 URLs.

You can leave this file as-is for any other API version, it works auto-magically.
"""

import os

from fastapi import APIRouter

from ..routing import find_and_add_endpoints

version_path_name = os.path.basename(os.path.dirname(os.path.abspath(__file__)))
version_prefixed = version_path_name.replace('_', '.')
version_prefix = f'/{version_prefixed}'  # Required for auto-routing

api_router = APIRouter()
find_and_add_endpoints(api_router, version_prefixed[1:])
