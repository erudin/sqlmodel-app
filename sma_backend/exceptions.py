# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Application exceptions."""


class CustomBaseException(Exception):  # noqa: N818
    """Generic custom base exception."""


class ValidationError(CustomBaseException):
    """Generic validation error."""
