"""SMA DB models."""

import typing

from sqlmodel import Field
from sqlmodel import Relationship
from sqlmodel import SQLModel


class MetricBase(SQLModel):
    """Base Class model for Metric."""

    description: str = Field(default=None, sa_column_kwargs={'unique': True})
    code: str


class Metric(MetricBase, table=True):
    """Class model for Metric database table."""

    id: typing.Optional[int] = Field(default=None, primary_key=True)  # noqa: A003

    value_definitions: typing.List['ValueDefinition'] = Relationship(
        back_populates='metric',
    )


class ValueDefinitionBase(SQLModel):
    """Base class model for Value Definition."""

    value_label: str
    value_type: str

    metric_id: int = Field(foreign_key='metric.id')


class ValueDefinition(ValueDefinitionBase, table=True):
    """Class model for Value Definition database table."""

    id: typing.Optional[int] = Field(default=None, primary_key=True)  # noqa: A003

    metric: Metric = Relationship(back_populates='value_definitions')
